﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RPG_Part_1;
using System.Windows.Forms;

namespace NewRPG {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
