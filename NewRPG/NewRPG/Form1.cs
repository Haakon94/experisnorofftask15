﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPG_Part_1;
using System.IO;

namespace NewRPG {
    public partial class Form1 : Form {

        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            characterDropDown.Items.Add("Wizard");
            characterDropDown.Items.Add("Dothraki");
            characterDropDown.Items.Add("Kingsguard");
            characterDropDown.Items.Add("Thief");
        }

        // Method for filling items to summary listbox
        public void FillListBox(Character rpgCharacter) {
            summaryBox.Items.Add($"Name: {rpgCharacter.Name}");
            summaryBox.Items.Add($"Type: {rpgCharacter.Type}");
            summaryBox.Items.Add($"HP: {rpgCharacter.Hp}");
            summaryBox.Items.Add($"Energy: {rpgCharacter.Energy}");
            summaryBox.Items.Add($"Armour rating: {rpgCharacter.ArmourRating}");
        }

        // Method for writing to file which take in Character class as parameter
        public void WriteFile(Character rpgchar) {
            // Sets path to default place in bin/debug and names file SummaryFile
            string path = "SummaryFile.txt";
            // Delete the file if it exists.
            if (File.Exists(path)) {
                File.Delete(path);
            }
            // Writes to file 
            using (StreamWriter sw = new StreamWriter(path)) {
                sw.WriteLine("SUMMARY");
                sw.WriteLine($"Name : {rpgchar.Name}, Type: {rpgchar.Type}, " +
                $"HP: {rpgchar.Hp}, Energy: {rpgchar.Energy}, Armour rating: {rpgchar.ArmourRating}");
            }
        }

        public void CharacterInsertSQL(Character rpgchar) {
            // Sets path to default place in bin/debug and names file SummaryFile
            string path = "CharacterInsertSQL.txt";
            // Delete the file if it exists.
            if (File.Exists(path)) {
                File.Delete(path);
            }
            // Writes to file 
            using (StreamWriter sw = new StreamWriter(path)) {
                sw.WriteLine("Character insert sql");
                // Also add to text file for task 15 the insert sqlite transaction 
                //sw.WriteLine("Insert transaction SQLite");
                sw.WriteLine($"INSERT INTO Character (Type,Name,Hp,Energy,ArmourRating) VALUES('{rpgchar.Type}','{rpgchar.Name}',{rpgchar.Hp},{rpgchar.Energy},{rpgchar.ArmourRating});");
            }
        }

        // Method submitButton_Click
        private void submitBtn_Click(object sender, EventArgs e) {
            try {
            // Takes in name from input box
            string characterName = nameInputBox.Text;

                #region fill out summary

                // Check which character is choosen, and then create object based on which character is chosen.
                // Calls then methods FillListBox and WriteFile with object as parameter
            if (characterDropDown.Text == "") {
                summaryBox.Items.Add("Please fill out information to get summary");

            } else if (characterDropDown.SelectedItem.ToString() == "Wizard") {
                Wizard wizard = new Wizard("Wizard", characterName, 100, 200, 300);
                FillListBox(wizard);
                WriteFile(wizard);
                CharacterInsertSQL(wizard);

            } else if (characterDropDown.SelectedItem.ToString() == "Dothraki") {
                DothrakiWarrior dothraki = new DothrakiWarrior("Dothraki", characterName, 200, 300, 400);
                FillListBox(dothraki);
                WriteFile(dothraki);
                CharacterInsertSQL(dothraki);

            } else if (characterDropDown.SelectedItem.ToString() == "Kingsguard") {
                KingsguardWarrior kingsguard = new KingsguardWarrior("Kingsguard", characterName, 300, 400, 500);
                FillListBox(kingsguard);
                WriteFile(kingsguard);
                CharacterInsertSQL(kingsguard);

            } else if (characterDropDown.SelectedItem.ToString() == "Thief") {
                Thief thief = new Thief("Thief", characterName, 50, 100, 200);
                FillListBox(thief);
                WriteFile(thief);
                CharacterInsertSQL(thief);
            }
                #endregion
            }
            catch(Exception ex) {

            }
        }

        // Method for adding to summary listbox
        public void CharacterAttack(string type) {
            summaryBox.Items.Add($"{type} attacked");
        }

        // Clear button
        private void clearBtn_Click(object sender, EventArgs e) {
            nameInputBox.Clear();
            summaryBox.Items.Clear();
        }

        // Attack click button just for experimenting
        private void Attack_Click(object sender, EventArgs e) {
            // Added method for attack button just for experimenting with switch statement
            // Checks which character is choosen and then calls method CharacterAttack
            try {
                switch (characterDropDown.SelectedItem.ToString()) {
                    case "Wizard":
                        CharacterAttack("Wizard");
                        break;
                    case "Dothraki":
                        CharacterAttack("Dothraki");
                        break;
                    case "Kingsguard":
                        CharacterAttack("Kingsguard");
                        break;
                    case "Thief":
                        CharacterAttack("Thief");
                        break;
                    default:
                        summaryBox.Items.Add("");
                        break;
                }
                

            } catch(Exception ex) {
                //MessageBox.Show(ex.Message);
                summaryBox.Items.Add("Please select an character");
                
            }
        }
    }
}
